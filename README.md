# ft_hexdump

## The goal of this function is to reproduce the `hexdump -C` command of the system.

#### To compile and use:
#### make && ./ft_hexdump [filename...]
